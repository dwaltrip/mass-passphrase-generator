
// https://stackoverflow.com/questions/4683539/variable-amount-of-nested-for-loops
module.exports = function doNestedLoops(limits, fn, opts={}) {
  var numLoops = limits.length;

  var indices = [];
  for (let i = numLoops - 1; i >= 0; i--) {
    if (limits[i] === 0) { return; }
    if (limits[i] < 0) { throw new Error('Loop limits must be positive integers'); }
    indices[i] = 0; // all the loops start with an index value of 0
  }

  var optNames = Object.keys(opts);
  optNames.forEach(name => {
    if (['startingPos', 'maxIterations'].indexOf(name) < 0) {
      throw new Error('Invalid option name:', name);
    }
  });

  if (opts.startingPos && opts.startingPos > 0) {
    // convert startingPos from a 1-based index to 0-based
    indices = getAdjustedStartingIndices(limits, opts.startingPos - 1);
  }

  var iterations = 0;

  // NOTE: I wonder if there is a way to write this code such that is simpler to understand
  // I didn't expect this to be as difficult as it was
  while (true) {
    var loopNum = numLoops - 1;

    fn(indices.slice());
    indices[numLoops - 1]++; // increment the innermost loop index
    iterations++; // we've just done one more iteration of the innermost loop

    if (opts.maxIterations && iterations === opts.maxIterations) { return; }

    // Find the loop that hasn't finished, looking from innermost to outermost
    //   Increment the index of this loop by 1
    //   Reset all the indices of the loops inside of it to 0 (as we need to do all of them again)
    // Also check to see if we have finished or not
    while(loopNum >= 0 && indices[loopNum] === limits[loopNum]) {
      if (loopNum === 0) {
        // Reaching this line means the current index for the outermost loop (loop 0) equals the specified limit
        // Thus we have finished all of the loops, and can exit
        return;
      }
      // At this point, the index for `loopNum` equals the specified limit, so we must:
      //   Reset current loop index to 0
      //   Increment the parent loop index by 1 (as we have completed one iteration)
      //   Set the current loop to be the parent loop
      indices[loopNum] = 0;
      indices[loopNum - 1]++;
      loopNum--;
    }
  }
};

// startingPos should be a 0-based index
function getAdjustedStartingIndices(limits, startingPos) {
  // For each loop, if it were made the outermost loop (ignoring any actual outer loops),
  // determine the number of total iterations of the inner that would occur
  var totalIterations = [];
  limits.forEach((limit, loopNum) => {
    let iterations = limit;
    for (let i = limits.length - 1; i > loopNum; i--) {
      iterations *= limits[i];
    }
    totalIterations.push(iterations);
  });

  var startingIndices = limits.map(_ => 0);
  var iterationsToAssign = startingPos;

  // Determine starting indices such that the overall starting iteration value matches the specified `startingPos`
  // This is very analagous to calculating the digits of a number. The main difference is that
  //    each digit slot (starting loop index) has a different base, thus the "place values" don't increase uniformly.
  //    We calculate these custom "place values" above, in the `totalIterations` array
  for (let i=1; i < totalIterations.length; i++) {
    let iterationsForLoop = totalIterations[i];

    if (iterationsForLoop <= iterationsToAssign) {
      let numLoopExecutions = Math.floor(iterationsToAssign / iterationsForLoop);
      startingIndices[i - 1] = numLoopExecutions;
      // The remainder is the amount left to assign into the indices of inner loops of the current one
      iterationsToAssign = iterationsToAssign % iterationsForLoop;
    }
  }
  // The logic is slightly different for the innermost loop
  if (iterationsToAssign > 0) {
    startingIndices[startingIndices.length - 1] = iterationsToAssign;
  }

  return startingIndices;
}
