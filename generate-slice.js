var getSliceSizes = require('./get-slice-sizes');
var doNestedLoops = require('./do-nested-loops');

const CHOOSE_ONE    = 'CHOOSE_ONE';
const INTEGER_RANGE = 'INTEGER_RANGE';
const FIXED         = 'FIXED';

// NOTE -- Other possibilities we may use:
// const CHAR_SERIES = 'CHAR_SERIES';
// const PERMUTE_ALL = 'PERMUTE_ALL';


module.exports = function generateSlice(partSpecs, numSlices, targetSliceIndex) {
  var numPossibilities = calcNumPossibilities(partSpecs);
  var sliceSizes = getSliceSizes(numPossibilities, numSlices);

  var startingPositions = [1]; // do-nested-loops requires 1-indexed startingPositions
  for (let i=1; i<numSlices; i++) {
    startingPositions.push(startingPositions[i-1] + sliceSizes[i-1]);
  }

  var possibilitiesPerPart = partSpecs.map(spec => numPossibilitiesForPart(spec));
  var startingPos = startingPositions[targetSliceIndex];
  var maxIterations = sliceSizes[targetSliceIndex];

  var slice = [];
  function addEntryToSlice(indices) {
    var partValues = indices.map((partIndex, i)=> getValueForPart(partSpecs[i], partIndex));
    slice.push(partValues.join(''));
  }

  doNestedLoops(possibilitiesPerPart, addEntryToSlice, { startingPos, maxIterations });
  return slice;
};

function calcNumPossibilities(partSpecs) {
  return partSpecs.reduce((total, spec) => total * numPossibilitiesForPart(spec), 1);
};
module.exports.calcNumPossibilities = calcNumPossibilities;


function getValueForPart(spec, index) {
  switch (spec.type) {
    case FIXED:         return spec.value;
    case CHOOSE_ONE:    return spec.options[index];
    case INTEGER_RANGE: return spec.start + index;
    default:
      throw new Error(`getValueForPart -- Invalid type: ${spec.type}`);
      break;
  }
};

function numPossibilitiesForPart(spec) {
  let num;
  switch (spec.type) {
    case FIXED:         num = 1; break;
    case CHOOSE_ONE:    num = spec.options.length; break;
    case INTEGER_RANGE: num = (spec.end - spec.start) + 1; break; // start & end are both inclusive
    default:
      throw new Error(`numPossibilitiesForPart -- Invalid type: ${spec.type}`);
      break;
  }
  if (isNaN(num) || num < 1 || Math.ceil(num) !== num) {
    throw new Error(`numPossibilitiesForPart -- Invalid # -- num: ${num} -- part spec: ${JSON.stringify(spec)}`);
  }
  return num;
}
