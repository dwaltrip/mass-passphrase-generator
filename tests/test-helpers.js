var { isEqual } = require('./utils');

var getSliceSizes = require('../get-slice-sizes');
var doNestedLoops = require('../do-nested-loops');

module.exports = {
  header: 'Helper functions',
  tests: [
    {
      header: 'Get slice sizes',
      tests: [
        {
          desc: 'Can divide 10 properly into 2 slices of even size',
          test: function() {
            var expectedSliceSizes = [5, 5];
            var slices = getSliceSizes(10, 2);
            return isEqual(slices, expectedSliceSizes);
          }
        }, {
          desc: 'Can divide 30 properly into 4 slices of mixed size',
          test: function() {
            var expectedSliceSizes = [8, 8, 7, 7];
            var slices = getSliceSizes(30, 4);
            return isEqual(slices, expectedSliceSizes);
          }
        }
      ]
    },
    {
      header: 'Arbitrary number of for loops',
      tests: [
        {
          desc: 'Execute an arbitrary number of for loops',
          test: function() {
            var forLoopLimits = [2,5,9]
            var expectedCount = forLoopLimits.reduce((memo, num) => memo * num, 1);

            var count = 0;
            doNestedLoops(forLoopLimits, ()=> { count++; });
            return isEqual(count, expectedCount);
          }
        }, {
          desc: 'Execute nested for loops, with adjusted starting index',
          test: function() {
            var forLoopLimits = [3, 5, 2]
            var startingPos = 23;
            var expectedIndices = [
              [2, 1, 0], [2, 1, 1],
              [2, 2, 0], [2, 2, 1],
              [2, 3, 0], [2, 3, 1],
              [2, 4, 0], [2, 4, 1]
            ];

            var loopIndices = [];
            doNestedLoops(forLoopLimits,
              indices => loopIndices.push(indices),
              { startingPos: startingPos }
            );
            return isEqual(loopIndices, expectedIndices);
          }
        }, {
          desc: 'Execute nested for loops, w/ starting index and limited iteration count',
          test: function() {
            var forLoopLimits = [7, 3, 10];
            var startingPos = 75;
            var sliceSize = 10;
            var expectedIndices = [
              [2, 1, 4], [2, 1, 5], [2, 1, 6], [2, 1, 7], [2, 1, 8],
              [2, 1, 9], [2, 2, 0], [2, 2, 1], [2, 2, 2], [2, 2, 3]
            ];

            var loopIndices = [];
            doNestedLoops(forLoopLimits,
              indices => loopIndices.push(indices),
              { startingPos: startingPos, maxIterations: sliceSize }
            );
            return isEqual(loopIndices, expectedIndices);
          }
        }
      ]
    }
  ]
};
