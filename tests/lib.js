var generateSlice = require('../generate-slice');
var { isEqual } = require('./utils');
var { assert, isEqual } = require('./utils');

module.exports.testSliceGeneration = function(parts, numSlices, expectedSlices) {
  assert(isArray(parts) && parts.length > 0, 'First argument must be a non-empty array');
  assert(parts[0].type, 'parts array should consist of JSON objects that specify a set of possibilities');
  assert((typeof numSlices === 'number') && numSlices > 0, 'Second argument must be a positive integer');
  assert(isArray(expectedSlices) && isArray(expectedSlices[0]) && (typeof expectedSlices[0][0] === 'string'),
    'expectedSlices argument is not valid');
  assert(numSlices === expectedSlices.length, 'numSlices must match the length of expectedSlices');

  for (var i=0; i<numSlices; i++) {
    var slice = generateSlice(parts, numSlices, i);
    if (!isEqual(slice, expectedSlices[i])) {
      return false;
    }
  }
  return true;
}

function isArray(val) {
  return Array.isArray(val);
}
