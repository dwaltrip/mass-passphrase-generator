
var TestRunner = require('./test-runner');

var tests = [
  require('./test-helpers'),
  require('./test-generate-slice'),
  require('./test-integer-range'),
];

if (require.main === module) {
  var testRunner = new TestRunner(tests);
  testRunner.run();
}
