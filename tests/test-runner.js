
module.exports = class TestRunner {
  constructor(tests=[], silentMode=false) {
    this.tests = tests;
    this.silentMode = silentMode;
  }

  run() {
    this.failureCount = 0;
    this.testCount = 0;
    this.groupCount = 0;

    this.printMessage('Starting tests...');
    this.tests.forEach(t => {
      if (isTestGroup(t)) {
        this.runGroup(t);
      } else {
        this.doTest(t);
      }
    });
    this.printMessage(`\nDone! Ran ${this.testCount} tests in total, from ${this.groupCount} main groups.`);
    this.printMessage(`Failures: ${this.failureCount}\n`);
  }

  runGroup(group, depth=0) {
    if (depth === 0) {
      this.groupCount++;
    }
    this.printTestInfo(group.header, depth);
    group.tests.forEach(t => {
      if (isTestGroup(t)) {
        this.runGroup(t, depth+1);
      } else {
        this.doTest(t, depth+1);
      }
    });
  }

  doTest(test, depth=1) {
    var error = null;
    try {
      var didPass = test.test();
    } catch (e) {
      error = e;
    }

    if (didPass) {
      this.printTestInfo(test.desc, depth);
    } else {
      if (error) {
        this.printTestInfo('FAIL -- ' + test.desc, depth);
        this.printTestInfo(error, depth+1);
      } else {
        this.printTestInfo('FAIL -- ' + test.desc, depth);
      }
      this.failureCount += 1;
    }
    this.testCount += 1;
  }

  printTestInfo(msg, depth=0) {
    var tabs = depth === 0 ? '\n' : '';
    for(let i=0; i<depth; i++) {
      tabs += '    ';
    }
    this.printMessage(tabs + msg);
  }

  printMessage(msg) {
    if (!this.silentMode) {
      console.log(msg);
    }
  }
};

function isTestGroup(t) {
  return typeof t.tests !== 'undefined';
}
