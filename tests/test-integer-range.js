var { testSliceGeneration } = require('./lib');

var parts = [
  /* part 1 */ { type: 'CHOOSE_ONE', options: ['foo', 'baz'] },
  /* part 2 */ { type: 'CHOOSE_ONE', options: ['_xxx_', '_zzz_'] },
  /* part 3 */ { type: 'INTEGER_RANGE', start: 10, end: 25 },
];

module.exports = {
  header: 'Generating slices for passphrases that have an INTEGER_RANGE type',
  tests: [
    {
      desc: 'Can properly generate slices when they nicely divide into the same size',
      test: function() {
        var data = getExpectedSlicesEven();
        return testSliceGeneration(parts, data.numSlices, data.expectedSlices);
      }
    }, {
      desc: 'Can properly generate slices wwhen they do not divide evenly',
      test: function() {
        var data = getExpctedSlicesMixed();
        return testSliceGeneration(parts, data.numSlices, data.expectedSlices);
      }
    }
  ]
};

function getExpctedSlicesMixed() {
  return {
    numSlices: 5,
    expectedSlices: [
      [
        'foo_xxx_10', 'foo_xxx_11', 'foo_xxx_12', 'foo_xxx_13', 'foo_xxx_14', 'foo_xxx_15', 'foo_xxx_16', 'foo_xxx_17',
        'foo_xxx_18', 'foo_xxx_19', 'foo_xxx_20', 'foo_xxx_21', 'foo_xxx_22',
      ], [
        'foo_xxx_23', 'foo_xxx_24', 'foo_xxx_25', 'foo_zzz_10', 'foo_zzz_11', 'foo_zzz_12', 'foo_zzz_13', 'foo_zzz_14',
        'foo_zzz_15', 'foo_zzz_16', 'foo_zzz_17', 'foo_zzz_18', 'foo_zzz_19',
      ], [
        'foo_zzz_20', 'foo_zzz_21', 'foo_zzz_22', 'foo_zzz_23', 'foo_zzz_24', 'foo_zzz_25', 'baz_xxx_10', 'baz_xxx_11',
        'baz_xxx_12', 'baz_xxx_13', 'baz_xxx_14', 'baz_xxx_15', 'baz_xxx_16',
      ], [
        'baz_xxx_17', 'baz_xxx_18', 'baz_xxx_19', 'baz_xxx_20', 'baz_xxx_21', 'baz_xxx_22', 'baz_xxx_23', 'baz_xxx_24',
        'baz_xxx_25', 'baz_zzz_10', 'baz_zzz_11', 'baz_zzz_12', 'baz_zzz_13',
      ], [
        'baz_zzz_14', 'baz_zzz_15', 'baz_zzz_16', 'baz_zzz_17', 'baz_zzz_18', 'baz_zzz_19', 'baz_zzz_20', 'baz_zzz_21',
        'baz_zzz_22', 'baz_zzz_23', 'baz_zzz_24', 'baz_zzz_25',
      ]
    ]
  };
};

function getExpectedSlicesEven() {
  return {
    numSlices: 4,
    expectedSlices: [
      [
        'foo_xxx_10', 'foo_xxx_11', 'foo_xxx_12', 'foo_xxx_13', 'foo_xxx_14', 'foo_xxx_15', 'foo_xxx_16', 'foo_xxx_17',
        'foo_xxx_18', 'foo_xxx_19', 'foo_xxx_20', 'foo_xxx_21', 'foo_xxx_22', 'foo_xxx_23', 'foo_xxx_24', 'foo_xxx_25',
      ], [
        'foo_zzz_10', 'foo_zzz_11', 'foo_zzz_12', 'foo_zzz_13', 'foo_zzz_14', 'foo_zzz_15', 'foo_zzz_16', 'foo_zzz_17', 
        'foo_zzz_18', 'foo_zzz_19', 'foo_zzz_20', 'foo_zzz_21', 'foo_zzz_22', 'foo_zzz_23', 'foo_zzz_24', 'foo_zzz_25',
      ], [
        'baz_xxx_10', 'baz_xxx_11', 'baz_xxx_12', 'baz_xxx_13', 'baz_xxx_14', 'baz_xxx_15', 'baz_xxx_16', 'baz_xxx_17',
        'baz_xxx_18', 'baz_xxx_19', 'baz_xxx_20', 'baz_xxx_21', 'baz_xxx_22', 'baz_xxx_23', 'baz_xxx_24', 'baz_xxx_25', 
      ], [
        'baz_zzz_10', 'baz_zzz_11', 'baz_zzz_12', 'baz_zzz_13', 'baz_zzz_14', 'baz_zzz_15', 'baz_zzz_16', 'baz_zzz_17',
        'baz_zzz_18', 'baz_zzz_19', 'baz_zzz_20', 'baz_zzz_21', 'baz_zzz_22', 'baz_zzz_23', 'baz_zzz_24', 'baz_zzz_25',
      ]
    ]
  };
}
