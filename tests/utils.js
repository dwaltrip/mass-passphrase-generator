

module.exports.assert = function(isValid, msg) {
  if (!isValid) {
    throw new Error(msg);
  }
};

module.exports.isEqual = function(left, right) {
  return JSON.stringify(left) === JSON.stringify(right);
}
