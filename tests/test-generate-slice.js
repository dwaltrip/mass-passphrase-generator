var { testSliceGeneration } = require('./lib');

var parts = [
  /* part 1 */ { type: 'CHOOSE_ONE', options: ['foo', 'baz'] },
  /* part 2 */ { type: 'CHOOSE_ONE', options: ['_mid_1_mid_', '_mid_2_mid_'] },
  /* part 3 */ { type: 'CHOOSE_ONE', options: ['one', 'two', 'three', 'four', 'five'] },
];

module.exports = {
  header: 'Generating slices for simple passphrases composed only from parts of type CHOOSE_ONE',
  tests: [
    {
      desc: 'Can properly generate slices when they nicely divide into the same size',
      test: function() {
        var data = getExpectedSlicesEven();
        return testSliceGeneration(parts, data.numSlices, data.expectedSlices);
      }
    }, {
      desc: 'Can properly generate slices when they do not divide evenly',
      test: function() {
        var data = getExpctedSlicesMixed();
        return testSliceGeneration(parts, data.numSlices, data.expectedSlices);
      }
    }
  ]
};

function getExpctedSlicesMixed() {
  return {
    numSlices: 6,
    expectedSlices: [
      [
        'foo_mid_1_mid_one',
        'foo_mid_1_mid_two',
        'foo_mid_1_mid_three',
        'foo_mid_1_mid_four',
      ], [
        'foo_mid_1_mid_five',
        'foo_mid_2_mid_one',
        'foo_mid_2_mid_two',
        'foo_mid_2_mid_three',
      ], [
        'foo_mid_2_mid_four',
        'foo_mid_2_mid_five',
        'baz_mid_1_mid_one',
      ], [
        'baz_mid_1_mid_two',
        'baz_mid_1_mid_three',
        'baz_mid_1_mid_four',
      ], [
        'baz_mid_1_mid_five',
        'baz_mid_2_mid_one',
        'baz_mid_2_mid_two',
      ], [
        'baz_mid_2_mid_three',
        'baz_mid_2_mid_four',
        'baz_mid_2_mid_five',
      ]
    ]
  };
};

function getExpectedSlicesEven() {
  return {
    numSlices: 4,
    expectedSlices: [
      [
        'foo_mid_1_mid_one',
        'foo_mid_1_mid_two',
        'foo_mid_1_mid_three',
        'foo_mid_1_mid_four',
        'foo_mid_1_mid_five',
      ], [
        'foo_mid_2_mid_one',
        'foo_mid_2_mid_two',
        'foo_mid_2_mid_three',
        'foo_mid_2_mid_four',
        'foo_mid_2_mid_five',
      ], [
        'baz_mid_1_mid_one',
        'baz_mid_1_mid_two',
        'baz_mid_1_mid_three',
        'baz_mid_1_mid_four',
        'baz_mid_1_mid_five',
      ], [
        'baz_mid_2_mid_one',
        'baz_mid_2_mid_two',
        'baz_mid_2_mid_three',
        'baz_mid_2_mid_four',
        'baz_mid_2_mid_five',
      ]
    ]
  };
}
