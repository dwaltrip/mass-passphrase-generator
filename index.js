
var generateSlice = require('./generate-slice');

module.exports = {
  generateSlice: generateSlice,
  calcNumPossibilities: generateSlice.calcNumPossibilities
};
