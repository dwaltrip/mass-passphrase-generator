
module.exports = function(totalSize, numSlices) {
  var sliceSize = Math.floor(totalSize / numSlices);
  var sizes = [];

  for (var i=0; i<numSlices; i++) {
    sizes.push(sliceSize);
  }

  var remainder = totalSize % numSlices;
  if (remainder > 0) {
    // `remainder` must be less than the number of slices, so to distribute it evenly over the slices
    //  we simply increase the sizes of the first `remainder` slices by 1
    for (var i=0; i<remainder; i++) {
      sizes[i] += 1
    }
  }

  return sizes;
};
